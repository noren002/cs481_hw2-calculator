﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace Calculator
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            output.Text = "";   
        }
        void OnNumber(object sender, System.EventArgs e)
        {
            output.Text += ((Button)sender).Text;
        }

        void OnOperator(object sender, System.EventArgs e)
        {
            output.Text += " " + ((Button)sender).Text + " ";
        }

        void OnClear(object sender, EventArgs e)
        {
            output.Text = "";
        }

        void OnEquals(object sender, EventArgs e)
        {  
            string[] equation = output.Text.Split(' '); 
            double num1 = Int32.Parse(equation[0]); 
            double num2 = Int32.Parse(equation[2]);
            string op = equation[1]; 
            output.Text = "";
            if (op == "+") output.Text = (num1 + num2).ToString();
            else if (op == "-") output.Text = (num1 - num2).ToString();
            else if (op == "x") output.Text = (num1 * num2).ToString();
            else if (op == "/") output.Text = (num1 / num2).ToString();
            else output.Text = "";            
        }
    }
}
